<?php

namespace app\modules\api\v1\models\payhandlers\stripe;

use app\modules\api\v1\components\Helper;
use app\modules\api\v1\exceptions\{
    StripeException,
    ValidationException
};
use app\modules\api\v1\models\forms\DepositForm;
use app\modules\api\v1\models\tables\{
    ExchangeDeposit,
    ExchangeTraderMembershipFee,
    ExchangeVendorMembershipFee
};
use Stripe\{
    Customer,
    Exception\ApiErrorException,
    Stripe,
    Subscription,
    Token,
    Invoice
};

class StripePayment
{
    public function createPayment($data)
    {
        try {
            $token = $this->createToken($data);
            Stripe::setApiKey(\Yii::$app->params['payments-setting']['stripe']['secret']);
            $customer = Customer::all([
                'email' => $data['email'],
                'limit' => 1,
            ]);
            if (empty($customer->data)) {
                $customer = Customer::create([
                    'email' => $data['email'],
                    'source' => $token,
                ]);
                $customerId = $customer->id;
            } else {
                $customerId = $customer->data[0]->id;
                $this->addCardCustomer($data, $customerId, $token);
            }

            $subs = Subscription::create([
                'customer' => $customerId,
                'items' => [
                    [
                        'price' => 'plan_day_05',
                    ],
                ],
            ]);
            if ($subs->status == 'incomplete') {
                self::cancelSubscription($subs->id);
                return ['subs' => $subs, 'status' => false];
            }
            return ['subs' => $subs, 'status' => true];
        } catch (ApiErrorException $exception) {
            throw new StripeException($exception->getMessage());
        }
    }

    private function createToken(array $data): Token
    {
        Stripe::setApiKey(\Yii::$app->params['payments-setting']['stripe']['public']);
        return Token::create([
            'card' => [
                'number' => $data['card_number'],
                'exp_month' => $data['card_month'],
                'exp_year' => $data['card_year'],
                'cvc' => $data['card_cvv'],
            ]
        ]);
    }

    public function addCardCustomer($params, $customerId, $token = false)
    {
        if (!$token) {
            $expiration = explode('/', $params['card_date']);
            $data = [
                'card_number' => $params['card_number'],
                'card_month' => $expiration[0],
                'card_year' => '20' . $expiration[1],
                'card_cvv' => $params['card_cvv'],
            ];
            $token = $this->createToken($data);
        }
        $customer = Customer::retrieve($customerId);
        $customer->source = $token->id;
        $customer->save();
    }

    public function repayment($params)
    {
        try {
            $deposit = ExchangeDeposit::findOne($params['id']);
            if (!$deposit)
                throw new ValidationException('Id is invalid');
            $description = explode('; ', $deposit->description); // TODO переделать, было архитектурно заложено ранее
            $invoiceId = $description[2];
            if (isset($params['card_number'])) {
                $this->addCardCustomer($params, $deposit->customer_id);
            }
            Stripe::setApiKey(\Yii::$app->params['payments-setting']['stripe']['secret']);
            $invoice = Invoice::retrieve($invoiceId);
            $invoice->pay();
            return true;
        } catch (ApiErrorException $exception) {
            throw new StripeException($exception->getMessage());
        }
    }

    public static function responseWebhook($data)
    {
        $customer = $data['data']['object']['customer'];
        $deposits = ExchangeDeposit::findAll(['customer_id' => $customer]);
        if (!$deposits)
            throw new ValidationException('Customer is invalid');
        $user = false;
        $invoice = $data['data']['object']['id'];
        foreach ($deposits as $deposit) {
            if (!$user)
                $user = $deposit->userAccount->user;
            $description = explode('; ', $deposit->description);
            if ($invoice == $description[2])
                return true;
        }
        if ($data['type'] == 'invoice.payment_failed') {
            DepositForm::createNewInvoice($user, $data, ExchangeDeposit::STATUS_REJECT);
            self::cancelSubscription($data['data']['object']['subscription']);
            if ($user->role == Helper::ROLE_VENDOR)
                ExchangeVendorMembershipFee::deleteAll(['user_id' => $user->id]);
            else
                ExchangeTraderMembershipFee::deleteAll(['user_id' => $user->id]);
        } elseif ($data['type'] == 'invoice.payment_succeeded') {
            DepositForm::createNewInvoice($user, $data, ExchangeDeposit::STATUS_CREATED);
        }
        return true;
    }

    public static function cancelSubscription($subsId)
    {
        $subscription = Subscription::retrieve($subsId);
        $subscription->cancel();
    }

    public static function cancelInvoice($invoiceId)
    {
        $invoice = Invoice::retrieve($invoiceId);
        $invoice->voidInvoice();
    }
}