<?php

namespace app\modules\api\v1\models\forms;

use app\modules\api\v1\components\{
    Helper,
    ExchangeHelper
};
use app\modules\api\v1\exceptions\ValidationException;
use app\modules\api\v1\models\payhandlers\stripe\StripePayment;
use app\modules\api\v1\ExchangeModule;
use app\modules\api\v1\models\tables\{
    ExchangeCurrency,
    ExchangeTraderMembershipFee,
    ExchangeUserAccount,
    ExchangeVendorMembershipFee,
    PaymentSystemSetting,
    ExchangeDeposit
};
use Yii;
use yii\helpers\ArrayHelper;

class DepositForm extends ExchangeDeposit
{
    const SCENARIO_CREATE_FIAT = 'create-fiat';
    const SCENARIO_CREATE_CRYPTO = 'create-crypto';

    const BANK_PAYMENT = 4;

    public $currency_id;
    public $card_number;
    public $card_date;
    public $card_cvv;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['amount', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number',
                'message' => ExchangeModule::t('model', 'Сумма должна быть больше 0')],
            ['amount', 'default', 'value' => 200],
            [['payment_system', 'currency_id', 'amount'], 'required'],
            [['amount'], 'checkAmountScope', 'on' => self::SCENARIO_CREATE_FIAT],

            ['amount', 'match', 'pattern' => '/^[0-9.]+$/u', 'message' => 'Only numbers and point allowed'],
            [['user_account_id', 'currency_id'], 'integer'],

            ['status', 'default', 'value' => self::STATUS_CREATED],
            [['card_number', 'card_date', 'card_cvv'], 'required', 'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            [['card_number', 'card_date', 'card_cvv'], 'filter', 'filter' => function ($value) {
                return str_replace(['_', ' '], '', trim($value));
            },
                'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            ['card_number', 'match', 'pattern' => '/\b(?:\d{4}[ -]?){3}\d{3,4}\b/', 'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            ['card_date', 'match', 'pattern' => '/^\d{2}\/\d{2}$/i', 'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            ['card_date', function ($attribute) {
                try {
                    date_default_timezone_set('Europe/Kiev');
                    $date = explode('/', $this->$attribute);
                    $date[1] = '20' . $date[1];
                    $newD = $date[1] . '-' . $date[0] . '-01';
                    $date = (new \DateTime($newD))->format('Y-m-d');
                    $timestamp = (new \DateTime($date))->getTimestamp();
                    if ((new \DateTime())->getTimestamp() > $timestamp)
                        $this->addError($attribute, 'Expiration date must be greater than the current date');
                } catch (\Exception $exception) {
                    $this->addError($attribute, 'Invalid date format');
                }
            }, 'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            ['card_cvv', 'match', 'pattern' => '/^\d{3,4}$/i', 'when' => function () {
                return $this->payment_system == self::BANK_PAYMENT;
            }],
            ['payment_system', 'checkCountTransaction']
        ];
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_CREATE_CRYPTO => ['payment_system', 'status'],
            self::SCENARIO_CREATE_FIAT => ['payment_system', 'amount', 'status'],
        ]);
    }

    public function checkCountTransaction()
    {
        $currentTimestamp = time();
        $startOfDayTimestamp = strtotime(date('Y-m-d 00:00:00', $currentTimestamp));
        $endOfDayTimestamp = strtotime(date('Y-m-d 23:59:59', $currentTimestamp));
        $userAccId = ExchangeUserAccount::myAccountId(1, Helper::getUserId());
        $data = self::find()
            ->where(['>=', 'created_at', $startOfDayTimestamp])
            ->andWhere(['<', 'created_at', $endOfDayTimestamp])
            ->andWhere(['user_account_id' => $userAccId]);
        if ($data->count() >= 3)
            throw new ValidationException('Daily request limit exceeded');
    }

    public function checkAmountScope()
    {
        if (!empty($this->payment_system) && $this->amount != '') {
            if (($paymentSetting = PaymentSystemSetting::find()->where([
                    'id' => intval($this->payment_system),
                    'status_deposit' => PaymentSystemSetting::STATUS_ACTIVE
                ])->one()) == null) {
                $this->addError('payment_system', ExchangeModule::t('model', 'Некорректная платежная система'));
            } else {
                $intAmount = ExchangeHelper::noFloatOfCurrencies((float)$this->amount);
                if ($intAmount < $paymentSetting->min_deposit_amount) {
                    $this->addError('amount', ExchangeModule::t('model', 'Минимальная сумма {amount} {currency}', [
                        'amount' => ExchangeHelper::floatOfCurrencies($paymentSetting->min_deposit_amount),
                        'currency' => $paymentSetting->currency->name
                    ]));
                } elseif ($intAmount > $paymentSetting->max_deposit_amount) {
                    $this->addError('amount', ExchangeModule::t('model', 'Максимальная сумма {amount} {currency}', [
                        'amount' => ExchangeHelper::floatOfCurrencies($paymentSetting->max_deposit_amount),
                        'currency' => $paymentSetting->currency->name
                    ]));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amount' => ExchangeModule::t('model', 'Сумма пополнения'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->amount != null) {
                $this->amount = ExchangeHelper::noFloatOfCurrencies($this->amount);
            }
            return true;
        }
        return false;
    }

    public function iseetUnpaidDeposit()
    {
        if (($model = self::find()->where([
                'user_account_id' => $this->user_account_id,
                'payment_system' => $this->payment_system,
                'status' => self::STATUS_CREATED
            ])->one()) != null) {
            return $model;
        }
        return false;
    }

    public static function createNewInvoice($user, $stripeData, $status)
    {
        $model = new ExchangeDeposit();
        $model->status = $status;
        $model->payment_system = self::BANK_PAYMENT;
        $model->customer_id = $stripeData['data']['object']['customer'];
        if ($status == self::STATUS_REJECT)
            $model->moder_comment = 'Problem with the card, one of the reasons is insufficient funds';
        $model->description = implode('; ', [
            $stripeData['data']['object']['subscription'],
            $stripeData['data']['object']['customer'],
            $stripeData['data']['object']['id'],
            $stripeData['data']['object']['status'],
        ]);
        $model->amount = ($user->role == Helper::ROLE_VENDOR) ?
            ExchangeVendorMembershipFee::MEMBERSHIP_FEE : ExchangeTraderMembershipFee::MEMBERSHIP_FEE;
        $model->amount = ExchangeHelper::noFloatOfCurrencies($model->amount);
        $model->user_account_id = ExchangeUserAccount::myAccountId(1, $user->id);
        if (!$model->save())
            throw new ValidationException(print_r($model->getErrorSummary(true), 1) ?? '');
        if ($status == self::STATUS_CREATED) {
            ExchangeDeposit::depositApprove($model->id);
        }
    }

    public function create()
    {

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->currency_id = 1;
            $this->amount = (Helper::getUserRole() == Helper::ROLE_VENDOR) ?
                ExchangeVendorMembershipFee::MEMBERSHIP_FEE : ExchangeTraderMembershipFee::MEMBERSHIP_FEE;
            $this->user_account_id = ExchangeUserAccount::myAccountId($this->currency_id);
            if (!$this->validate()) {
                throw new ValidationException('Validate error');
            }
            $paymentManager = Yii::$app->paymentManager;
            if (!$this->save()) {
                throw new ValidationException('Save error');
            }
            if (($currency = ExchangeCurrency::findOne($this->currency_id)) == null) {
                throw new ValidationException('Currency error');
            }
            if (!$this->userAccount) {
                ExchangeUserAccount::newAccount(Helper::getUserId());
            }
            $this->checkUserAvailablePay($this->userAccount);
            if ($this->paymentSystemSetting->paymentsystem->pay_handler == 'paypal') {
                if (!$data = $paymentManager->getDepositForm($this->payment_system, $this->id, ExchangeHelper::floatOfCurrencies($this->amount), $currency->name)) {
                    throw new ValidationException('Deposit form error');
                }
            } elseif ($this->paymentSystemSetting->paymentsystem->pay_handler == 'bank') {
                $payment = new StripePayment();
                $expiration = explode('/', $this->card_date);
                $data = [
                    'card_number' => $this->card_number,
                    'card_month' => $expiration[0],
                    'card_year' => '20' . $expiration[1],
                    'card_cvv' => $this->card_cvv,
                    'amount' => ExchangeHelper::floatOfCurrencies($this->amount),
                    'email' => $this->userAccount->user->email,
                    'name' => $this->userAccount->user->first_name . ' ' . $this->userAccount->user->last_name,
                ];
                $res = $payment->createPayment($data);
                $cardInfo = [
                    'subs_id' => $res['subs']->id,
                    'customer' => $res['subs']->customer,
                    'latest_invoice' => $res['subs']->latest_invoice,
                    'status' => $res['subs']->status
                ];
                $this->description = implode('; ', $cardInfo);
                $this->amount = ExchangeHelper::floatOfCurrencies($this->amount);
                $this->customer_id = $res['subs']->customer;
                if ($res['status']) {
                    $this->save();
                    ExchangeDeposit::depositApprove($this->id);
                    if (Helper::getUserRole() == Helper::ROLE_VENDOR)
                        ExchangeVendorMembershipFee::makeMembership();
                    else
                        ExchangeTraderMembershipFee::makeMembership();
                    $data = ['status' => true, 'message' => ''];
                } else {
                    $data = ['status' => false, 'message' => 'Problem with the card, one of the reasons is insufficient funds'];
                    $this->moder_comment = $data['message'];
                    $this->status = self::STATUS_REJECT;
                    $this->save();
                }
            } else
                throw new ValidationException('Payment system invalid');

            $transaction->commit();
            return $data;

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ValidationException($e->getMessage());
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw new ValidationException($e->getMessage());
        }
    }

    private function checkUserAvailablePay($userAcc)
    {
        $message = 'You will need to fill out your profile completely to pay for your membership.';
        if (Helper::getUserRole() == Helper::ROLE_TRADER && is_null($userAcc->user->addresses))
            throw new ValidationException($message);
        elseif (Helper::getUserRole() == Helper::ROLE_VENDOR && (!isset($userAcc->farmInfo->name) || is_null($userAcc->farmInfo->name)))
            throw new ValidationException($message);
    }

    public static function createLocalDeposit($amount)
    {
        $model = new self();
        $model->payment_system = 5;
        $model->currency_id = 1;
        $model->amount = $amount;
        $model->user_account_id = ExchangeUserAccount::myAccountId($model->currency_id);
        $model->save();
        self::depositApprove($model->id);
    }

    public function checkDeposit($id)
    {
        $deposit = self::findOne($id);
        if (!$deposit)
            throw new ValidationException('Deposit is invalid');
        $userAccId = ExchangeUserAccount::myAccountId(1);
        if ($deposit->user_account_id != $userAccId)
            throw new ValidationException('User is invalid');
    }

    public static function paypalResponse($data)
    {
        $id = $data['invoice'] ?? null;
        $model = self::findOne($id);
        if (!$id)
            throw new ValidationException('Invoice is invalid');
        if (strtolower($data['payment_status']) == 'completed') {
            $approve = false;
            if ($model->status == self::STATUS_CREATED)
                $approve = true;
            if (!isset($data['txn_id'])) {
                if (is_null($model->description))
                    $model->description = 'Transaction paid';
            } else {
                $responseData = [
                    'payer_id' => 'payer_id: '.$data['payer_id'],
                    'payer_email' => 'payer_email: '.$data['payer_email'],
                    'txn_id' => 'txn_id: '.$data['txn_id'],
                    'receiver_id' => 'receiver_id: '.$data['receiver_id'],
                    'ipn_track_id' => 'ipn_track_id: '.$data['ipn_track_id'],
                ];
                $model->description = implode('; ', $responseData);
            }
            $model->amount = ExchangeHelper::floatOfCurrencies($model->amount);
            $model->currency_id = 1;
            $model->save();
            if ($approve) {
                ExchangeDeposit::depositApprove($model->id);
                if ($model->userAccount->user->role == Helper::ROLE_VENDOR)
                    ExchangeVendorMembershipFee::makeMembership($model->userAccount->user->id);
                else
                    ExchangeTraderMembershipFee::makeMembership($model->userAccount->user->id);
            }
        } elseif (strtolower($data['payment_status']) == 'cancel') {
            $model->amount = ExchangeHelper::floatOfCurrencies($model->amount);
            $model->currency_id = 1;
            $model->description = 'Transaction canceled';
            $model->status = self::STATUS_REJECT;
            $model->save();
        } else
            throw new ValidationException('Invalid status');

        return true;
    }

}
